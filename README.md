-- Useful links

Setup:
# Chapter "Starting up MariaDB MaxScale and MariaDB Server"
https://mariadb.com/resources/blog/getting-started-mariadb-maxscale-database-firewall-filter

Masking:
https://mariadb.com/resources/blog/sensitive-data-masking-mariadb-maxscale
https://mariadb.com/kb/en/mariadb-enterprise/mariadb-maxscale-21-masking/

`For a secure solution, the masking filter must be combined with the firewall filter to prevent the use of functions using which the masking can be bypassed.`

Database firewall:
https://mariadb.com/kb/en/mariadb-enterprise/mariadb-maxscale-21-database-firewall-filter/